<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="https://akirapublicidad.com/vistas/css/fonts/rockb/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="confirm.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>WhatATour</title>
<script>
function closeMe()
{
	window.open('https://www.google.com.mx', '_self', '');
	window.close();
}
</script>
</head>
<body>
	<div id="container">
		<div id="titulo"><div id="header"><img src="logo.png" alt=""></div></div>
		<div id="estetico">
		<p class="enbreve">En breve uno de nuestros Agentes te atenderá para brindarte el servicio.</p>
		<form id="formWhatatour" method="post">
			<div class="preguntas_group">
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-user"></i></span>
				<input placeholder="Nombre *" class="data" name="nombre" id="nombre" type="text">
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-globe"></i></span>
				<input placeholder="¿A Donde Quieres Viajar? *" class="data" name="destino" id="destino" type="text" required>
			</div>
			<div id="desc" class="pregunta">
				<span><i class="glyphicon glyphicon-globe"></i></span>
				<textarea placeholder="Describe brevemente como deseas tu viaje (Actividades, días, No. de personas, etc). *" class="data" name="descripcion" id="descripcion" cols="30" rows="10" required></textarea>
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-time"></i></span>
				<input placeholder="Fecha tentativa de tu viaje" class="data" id="fecha" name="fecha" required="" type="text" onfocus="(this.type='date')"/>
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-time"></i></span>
				<input placeholder="Fecha de regreso" class="data" id="fecha" name="fecha" required="" type="text" onfocus="(this.type='date')"/>
			</div>			
			</div>
			<div class="preguntas_group">
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-user"></i></span>
				<input placeholder="Numero de personas" class="data" id="personas" name="personas" required="" type="number"/>
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-time"></i></span>
				<input placeholder="¿Cuántos días requieres?" class="data" id="dias" name="dias" required="" type="number"/>
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-earphone"></i></span>
				<input placeholder="Teléfono *" class="data" id="tel" name="tel" required="" type="tel"/>
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-envelope"></i></span>
				<input placeholder="Correo electrónico *" class="data" id="email" name="email" required="" type="email"/>
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-home"></i></span>
				<input placeholder="Ciudad" class="data" name="ciudad" id="ciudad" type="text" required>
			</div>
			<div class="pregunta">
				<span><i class="glyphicon glyphicon-home"></i></span>
				<input placeholder="Código Postal" class="data" name="cp" id="cp" type="number">
			</div>
			</div>
			<input name="enviar" id="enviar" type="submit" value="Enviar solicitud">
		</form>
		</div>
				<footer>
			<div id="footer">
				<div>
					<input name="cerrar" id="cerrar" type="button" value="Cerrar" onclick="closeMe()">
				</div>
			</div>
		</footer>
	</div>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script>
		$("#enviar").on("click", function() {
		    var datos = $('#formWhatatour').serialize();
		    $.ajax({
		        type: "POST",
		        url: "enviar.php",
		        data: datos,
		        success: function(resp) {
		        	if (resp == true) {
		            	$('#estetico').empty();
		            	$('#estetico').html('<div id="confirm"><div><img id="check" src="portada.jpg" alt=""><div><p class="confText">Gracias por tu mensaje</p><p class="confText">en breve un agente<br>se pondrá en contacto contigo.</p><button id="text_btn" onclick="redirect();">Visitar Sitio Web</button></div></div></div>');
		            }
		            else{
		        		swal({
		            	 icon: "error",
		            	 title: "Oops...", 
		            	 text: "El correo no se envio, revisa que hayas llenado todos los datos con *",})
		            }
		        }
		    });
		    return false;
		});
	</script>
	<script type="text/javascript">
		function redirect(){
			window.location.href="http://www.whatatour.com";
		}
	</script>
</body>
</html>

<!-- 1152 -->